import { Component, OnInit } from '@angular/core';

import Geolocation from 'ol/Geolocation.js';
import Map from 'ol/Map.js';
import View from 'ol/View.js';
import Point from 'ol/geom/Point.js';
import Feature from 'ol/Feature';
import { fromLonLat } from 'ol/proj';
import OlPoint from 'ol/geom/Point';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';
import {defaults as defaultInteractions, PinchZoom} from 'ol/interaction';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style.js';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {
  latitude: number;
  longitude: number;
  marker: Feature;
  constructor() {
    this.latitude = 48.1829806;
    this.longitude = 16.3645168;
    this.marker = new Feature({
      type: 'geoMarker',      
      geometry: new OlPoint(fromLonLat([this.longitude, this.latitude]))
    });
  }

  ngOnInit() {
    var checked = false;
    var view = new View({
      center: fromLonLat([this.longitude, this.latitude]),
      zoom: 16
    });
    
    var map = new Map({
      interactions: defaultInteractions({pinchZoom: false}).extend([
        new PinchZoom({
          constrainResolution: true // force zooming to a integer zoom
        })
      ]),
      layers: [
        new TileLayer({
          source: new OSM()
        })
      ],
      target: 'map',
      view: view
    });
    
    var geolocation = new Geolocation({
      // enableHighAccuracy must be set to true to have the heading value.
      trackingOptions: {
        enableHighAccuracy: true
      },
      projection: view.getProjection()
    });
    
    function el(id) {
      return document.getElementById(id);
    }

    el('track').addEventListener('change', function() {
      checked =! checked;
      geolocation.setTracking(checked);
    });
    
    // handle geolocation error.
    geolocation.on('error', function(error) {
      alert(error.message);
    });
    
    var positionFeature = new Feature();
    positionFeature.setStyle(new Style({
      image: new CircleStyle({
        radius: 6,
        fill: new Fill({
          color: '#3399CC'
        }),
        stroke: new Stroke({
          color: '#fff',
          width: 4
        })
      })
    }));
    
    geolocation.on('change:position', function() {
      var coordinates = geolocation.getPosition();
      positionFeature.setGeometry(coordinates ?
        new Point(coordinates) : null);
    });
    
    var vectorLayer = new VectorLayer({
      map: map,
      source: new VectorSource({
        features: [positionFeature]
      })
    });
  }
}
